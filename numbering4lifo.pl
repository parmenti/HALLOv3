#! /usr/bin/perl
#
# Script used to extend the bibitem extracted from HAL 
# for jabref's layout
#
# This file is part of HALLO.
#
# HALLO is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# HALLO is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with HALLO.  If not, see <http://www.gnu.org/licenses/>.
#
# date: 2010/04/12
# author: Y.Parmentier
#

use strict;
use warnings;
use File::Basename;
use Getopt::Std;
use POSIX qw/strftime/;

#####################################################
# FUNCTIONS:

sub main::HELP_MESSAGE()  
{
    print "Usage:\n  perl $0 -i input_tex [-n names] [-p prefix] [-w] [-d]\n\n";
    print "where:\n";
    print "-n: names to underline \n";
    print "-w: with empty headers \n";
    print "-d: to encode accents in latex \n";
    print "\n";
}

sub main::VERSION_MESSAGE()
{
    print "$0 version 1.0 - April 2010\n";
}

sub latexdecode 
{
    my ($x, $dec) = @_;
    if ( !$dec ) {
	# if dec == 0
	# that is, we want accents
	#lowercase
	$x =~ s/\{\\`a\}/à/g; 
	$x =~ s/\{\\'a\}/á/g; 
	$x =~ s/\{\\\^a\}/â/g; 
	$x =~ s/\{\\"a\}/ä/g; 
	$x =~ s/\{\\c c\}/c/g; 
	$x =~ s/\{\\'e\}/é/g; 
	$x =~ s/\{\\`e\}/è/g; 
	$x =~ s/\{\\\^e\}/ê/g; 
	$x =~ s/\{\\"e\}/ë/g; 
	$x =~ s/\{\\`i\}/ì/g; 
	$x =~ s/\{\\'i\}/í/g; 
	$x =~ s/\{\\\^\\i\}/î/g;
	$x =~ s/\{\\"i\}/ï/g; 
	$x =~ s/\{\\`o\}/ò/g;  
	$x =~ s/\{\\'o\}/ó/g;  
	$x =~ s/\{\\\^o\}/ô/g; 
	$x =~ s/\{\\"o\}/ö/g;
	$x =~ s/\{\\`u\}/ù/g;  
	$x =~ s/\{\\'u\}/ú/g;  
	$x =~ s/\{\\\^u\}/û/g;  
	$x =~ s/\{\\"u\}/ü/g;  
	#uppercase
	$x =~ s/\{\\`A\}/À/g; 
	$x =~ s/\{\\'A\}/Á/g; 
	$x =~ s/\{\\\^A\}/Â/g; 
	$x =~ s/\{\\"A\}/Ä/g; 
	$x =~ s/\{\\c c\}/C/g; 
	$x =~ s/\{\\'e\}/É/g; 
	$x =~ s/\{\\`e\}/È/g; 
	$x =~ s/\{\\\^e\}/Ê/g; 
	$x =~ s/\{\\"e\}/Ë/g; 
	$x =~ s/\{\\`i\}/Ì/g; 
	$x =~ s/\{\\'i\}/Í/g; 
	$x =~ s/\{\\\^\\i\}/Î/g;
	$x =~ s/\{\\"i\}/Ï/g; 
	$x =~ s/\{\\`o\}/Ò/g;  
	$x =~ s/\{\\'o\}/Ó/g;  
	$x =~ s/\{\\\^o\}/Ô/g; 
	$x =~ s/\{\\"o\}/Ö/g;
	$x =~ s/\{\\`u\}/Ù/g;  
	$x =~ s/\{\\'u\}/Ú/g;  
	$x =~ s/\{\\\^u\}/Û/g;  
	$x =~ s/\{\\"u\}/Ü/g;  
	# maybe the commands are not within {}, cf JabRef
	#lowercase
	$x =~ s/\\`a/à/g; 
	$x =~ s/\\'a/á/g; 
	$x =~ s/\\\^a/â/g; 
	$x =~ s/\\"a/ä/g; 
	$x =~ s/\\c c/c/g; 
	$x =~ s/\\'e/é/g; 
	$x =~ s/\\`e/è/g; 
	$x =~ s/\\\^e/ê/g; 
	$x =~ s/\\"e/ë/g; 
	$x =~ s/\\`i/ì/g; 
	$x =~ s/\\'i/í/g; 
	$x =~ s/\\\^\\i/î/g;
	$x =~ s/\\"i/ï/g; 
	$x =~ s/\\`o/ò/g;  
	$x =~ s/\\'o/ó/g;  
	$x =~ s/\\\^o/ô/g; 
	$x =~ s/\\"o/ö/g;
	$x =~ s/\\`u/ù/g;  
	$x =~ s/\\'u/ú/g;  
	$x =~ s/\\\^u/û/g;  
	$x =~ s/\\"u/ü/g;  
	#uppercase
	$x =~ s/\\`A/À/g; 
	$x =~ s/\\'A/Á/g; 
	$x =~ s/\\\^A/Â/g; 
	$x =~ s/\\"A/Ä/g; 
	$x =~ s/\\c c/C/g; 
	$x =~ s/\\'e/É/g; 
	$x =~ s/\\`e/È/g; 
	$x =~ s/\\\^e/Ê/g; 
	$x =~ s/\\"e/Ë/g; 
	$x =~ s/\\`i/Ì/g; 
	$x =~ s/\\'i/Í/g; 
	$x =~ s/\\\^\\i/Î/g;
	$x =~ s/\\"i/Ï/g; 
	$x =~ s/\\`o/Ò/g;  
	$x =~ s/\\'o/Ó/g;  
	$x =~ s/\\\^o/Ô/g; 
	$x =~ s/\\"o/Ö/g;
	$x =~ s/\\`u/Ù/g;  
	$x =~ s/\\'u/Ú/g;  
	$x =~ s/\\\^u/Û/g;  
	$x =~ s/\\"u/Ü/g;  
    }
    return $x;
}

sub latexencode 
{
    my ($x, $dec) = @_;
    #in case the input bib contains accents,
    #we encode them with their latex counterparts
    if ( $dec ) {
	#lowercase
	$x =~ s/à/\{\\`a\}/g; 
	$x =~ s/á/\{\\'a\}/g; 
	$x =~ s/â/\{\\\^a\}/g; 
	$x =~ s/ä/\{\\"a\}/g; 
	$x =~ s/ç/\{\\c c\}/g; 
	$x =~ s/é/\{\\'e\}/g; 
	$x =~ s/è/\{\\`e\}/g; 
	$x =~ s/ê/\{\\\^e\}/g; 
	$x =~ s/ë/\{\\"e\}/g; 
	$x =~ s/ì/\{\\`i\}/g; 
	$x =~ s/í/\{\\'i\}/g; 
	$x =~ s/î/\{\\\^\\i\}/g;
	$x =~ s/ï/\{\\"i\}/g; 
	$x =~ s/ò/\{\\`o\}/g;  
	$x =~ s/ó/\{\\'o\}/g;  
	$x =~ s/ô/\{\\\^o\}/g; 
	$x =~ s/ö/\{\\"o\}/g;
	$x =~ s/ù/\{\\`u\}/g;  
	$x =~ s/ú/\{\\'u\}/g;  
	$x =~ s/û/\{\\\^u\}/g;  
	$x =~ s/ü/\{\\"u\}/g;  
	#uppercase
	$x =~ s/À/\{\\`A\}/g; 
	$x =~ s/Á/\{\\'A\}/g; 
	$x =~ s/Â/\{\\\^A\}/g; 
	$x =~ s/Ä/\{\\"A\}/g; 
	$x =~ s/Ç/\{\\c C\}/g; 
	$x =~ s/É/\{\\'e\}/g; 
	$x =~ s/È/\{\\`e\}/g; 
	$x =~ s/Ê/\{\\\^e\}/g; 
	$x =~ s/Ë/\{\\"e\}/g; 
	$x =~ s/Ì/\{\\`i\}/g; 
	$x =~ s/Í/\{\\'i\}/g; 
	$x =~ s/Î/\{\\\^\\i\}/g;
	$x =~ s/Ï/\{\\"i\}/g; 
	$x =~ s/Ò/\{\\`o\}/g;  
	$x =~ s/Ó/\{\\'o\}/g;  
	$x =~ s/Ô/\{\\\^o\}/g; 
	$x =~ s/Ö/\{\\"o\}/g;
	$x =~ s/Ù/\{\\`u\}/g;  
	$x =~ s/Ú/\{\\'u\}/g;  
	$x =~ s/Û/\{\\\^u\}/g;  
	$x =~ s/Ü/\{\\"u\}/g;  
	# maybe the commands are not within {}, cf JabRef
	#lowercase
	$x =~ s/à/\\`a/g; 
	$x =~ s/á/\\'a/g; 
	$x =~ s/â/\\\^a/g; 
	$x =~ s/ä/\\"a/g; 
	$x =~ s/ç/\\c c/g; 
	$x =~ s/é/\\'e/g; 
	$x =~ s/è/\\`e/g; 
	$x =~ s/ê/\\\^e/g; 
	$x =~ s/ë/\\"e/g; 
	$x =~ s/ì/\\`i/g; 
	$x =~ s/í/\\'i/g; 
	$x =~ s/î/\\\^\\i/g;
	$x =~ s/ï/\\"i/g; 
	$x =~ s/ò/\\`o/g;  
	$x =~ s/ó/\\'o/g;  
	$x =~ s/ô/\\\^o/g; 
	$x =~ s/ö/\\"o/g;
	$x =~ s/ù/\\`u/g;  
	$x =~ s/ú/\\'u/g;  
	$x =~ s/û/\\\^u/g;  
	$x =~ s/ü/\\"u/g;  
	#uppercase
	$x =~ s/À/\\`A/g; 
	$x =~ s/Á/\\'A/g; 
	$x =~ s/Â/\\\^A/g; 
	$x =~ s/Ä/\\"A/g; 
	$x =~ s/Ç/\\c c/g; 
	$x =~ s/É/\\'e/g; 
	$x =~ s/È/\\`e/g; 
	$x =~ s/Ê/\\\^e/g; 
	$x =~ s/Ë/\\"e/g; 
	$x =~ s/Ì/\\`i/g; 
	$x =~ s/Í/\\'i/g; 
	$x =~ s/Î/\\\^\\i/g;
	$x =~ s/Ï/\\"i/g; 
	$x =~ s/Ò/\\`o/g;  
	$x =~ s/Ó/\\'o/g;  
	$x =~ s/Ô/\\\^o/g; 
	$x =~ s/Ö/\\"o/g;
	$x =~ s/Ù/\\`u/g;  
	$x =~ s/Ú/\\'u/g;  
	$x =~ s/Û/\\\^u/g;  
	$x =~ s/Ü/\\"u/g;  
    }
    return $x;
}
#
########################################################

my $time = strftime('%d-%b-%Y %H:%M',localtime); 

$Getopt::Std::STANDARD_HELP_VERSION = 1;

my %opts;
getopts('i:n:p:wd', \%opts);
# i input rep/fil
# p prefix
# w with empty section headers
# d decoding accents

if ( !(exists $opts{'i'}) ) {
    print STDERR "** Error: input file is missing. **\n";
    main::HELP_MESSAGE();
    exit 1;
}

# comments
my $empty = 0; #false by default
# prefix
my $prefix = "";
# names
my $all = "";
# decode
my $dec = 0;

if ( defined($opts{'w'}) ) {
    $empty = 1; #true
}
if ( defined($opts{'d'}) ) {
    $dec = 1; #true
}
if ( exists($opts{'p'}) ) {
    $prefix = $opts{'p'}."-";
}
if ( exists($opts{'n'}) ) {
    $all = $opts{'n'};
}

# input file
my $file = $opts{'i'};

my $l; #line
my $curType; #entry type
my $curCode; #for section headers

my %int2month = 
    (
     '01' => "Janvier", 
     '02' => latexdecode("F{\\'e}vrier",$dec),
     '03' => "Mars",
     '04' => "Avril",
     '05' => "Mai",
     '06' => "Juin",
     '07' => "Juillet",
     '08' => latexdecode("Ao{\\^u}t",$dec),
     '09' => "Septembre",
     '10' => "Octobre",
     '11' => "Novembre",
     '12' => latexdecode("D{\\'e}cembre",$dec)
    );

my %code2name = 
    (
     '(a)'   => latexdecode("Articles dans des revues internationales avec comit{\\'e} de lecture r{\\'e}pertori{\\'e}es", $dec),
     '(b)'   => latexdecode("Articles dans des revues nationales avec comit{\\'e} de lecture r{\\'e}pertori{\\'e}es", $dec),
     '(c)'   => latexdecode("Articles dans des revues internationales sans comit{\\'e} de lecture", $dec),
     '(d)'   => latexdecode("Articles dans des revues nationales sans comit{\\'e} de lecture", $dec),
     '(e)'   => "Direction d\'ouvrages scientifiques",
     '(f)'   => "Chapitres d\'ouvrages scientifiques",
     '(g)'   => "Actes de colloques",
     '(h)'   => latexdecode("Th{\\`e}ses et Habilitations {\\`a} Diriger des Recherches", $dec),
     '(i)'   => latexdecode("Conf{\\'e}rences donn{\\'e}es {\\`a} l'invitation du Comit{\\'e} d'organisation dans un congr{\\`e}s international ou national", $dec),
     '(j)'   => latexdecode("Communications avec actes dans un congr{\\`e}s international", $dec),
     '(k)'   => latexdecode("Communications avec actes dans un congr{\\`e}s national", $dec),
     '(l)'   => latexdecode("Communications par affiche dans un congr{\\`e}s international ou national", $dec),
     '(m)'   => "Communications orales sans actes dans un congr{\\`e}s international ou national",
     '(n)'   => "Autres Publications"
    );

my %code2int = 
    (
     '(a)'   => 0,
     '(b)'   => 1,
     '(c)'   => 2,
     '(d)'   => 3,
     '(e)'   => 4,
     '(f)'   => 5,
     '(g)'   => 6,
     '(h)'   => 7,
     '(i)'   => 8,
     '(j)'   => 9,
     '(k)'   => 10,
     '(l)'   => 11,
     '(m)'   => 12,
     '(n)'   => 13,
    );

my %int2code = 
    (
     0 => '(a)',
     1 => '(b)',
     2 => '(c)',
     3 => '(d)',
     4 => '(e)',
     5 => '(f)',
     6 => '(g)',
     7 => '(h)',
     8 => '(i)',
     9 => '(j)',
     10=> '(k)',
     11=> '(l)',
     12=> '(m)',
     13=> '(n)',
    );

# to store the statistics
my %pertype = 
    (
     'ACL'    => 0,
     'ACL(N)' => 0,
     'ASCL'   => 0,
     'ASCL(N)'=> 0,
     'DO'     => 0,
     'OS'     => 0,
     'PROC'   => 0,
     'TH'     => 0,
     'INV'    => 0,
     'ACTI'   => 0,
     'ACTN'   => 0,
     'AFF'    => 0,
     'COM'    => 0,
     'AP'     => 0
    ); 
my %peryear = ();

my @orderedtypes = ('ACL', 'ACL(N)', 'ASCL', 'ASCL(N)', 'DO', 'OS', 'PROC', 'TH', 'INV', 'ACTI', 'ACTN', 'AFF', 'COM', 'AP');

my @suffixes;
push(@suffixes, '.txt');
push(@suffixes, '.tex');
push(@suffixes, '.pre');
push(@suffixes, '.req');

my ($filename,$path) = fileparse($file,@suffixes);
my $out = $path.$filename.".tex";
my $stats = $path.$filename.".stats";

my $cpt = 1;
my $sectionCpt = 0;

if (-e $out) {
    (unlink $out) or die("Error, cannot remove $out : $!");
}
if (-e $stats) {
    (unlink $stats) or die("Error, cannot remove $stats : $!");
}

open(NAMES, "<$all") or die("Error while opening $all : $!");
my @allnames = <NAMES>;
#print @allnames;
close(NAMES);

open(INFILE,"<$file")  or die("Error while opening $file : $!");
open(OUTFILE,">>$out") or die("Error while opening $out : $!");
open(STATS,">>$stats") or die("Error while opening $stats : $!");

print OUTFILE "%% Generated using $0 on $time\n";
print STATS "%% Generated using $0 on $time\n";
#print STATS "\nInput file : ".$file."\n\n";

while( defined( $l = <INFILE> ) )
{
    $l =~ s/[^\\]&/\\&/g;
    $l =~ s/\\vS/\\v\{S\}/g; # for \v{S}koviera

    $l = latexdecode($l, $dec);
    $l = latexencode($l, $dec);

    if ($l =~ /\\begin\{lifo.bib\}\{\}/) {
	<INFILE>; # thus, we remove the first empty lifo.bib env
    }
    elsif ($l =~ /^$/) {
	# to remove blank lines
    }
    elsif ($l =~ /\\end\{/) {
	print OUTFILE $l."\n";
    }
    elsif ($l =~ /begin\{lifo.bib\}.*%%(.*)$/) {
	$curCode = $1;
	#print $curCode."\n";
	if ( $empty ) {
	    my $j = $sectionCpt;
	    #print " ".$j." ".$code2int{$curCode}."\n";
	    for (; $j < $code2int{$curCode} ; $j++) {
		print OUTFILE '\begin{lifo.bib.empty}{'.$code2name{$int2code{$j}}."}\n";
		print OUTFILE '\end{lifo.bib.empty}'."\n\n";
	    }
	    $sectionCpt=$j+1;
	}
	print OUTFILE $l;
    }
    elsif ($l =~ m/^\\bibcitekey\{([^\-\s]*)\-\} %%(.*)$/) {
	$curType = $1;
	#$curType =~ s/ACL\(N\)/ACL/g;
	#$curType =~ s/ASCL\(N\)/ASCL/g;
	my $ccode= $2;
	print OUTFILE '\bibcitekey{'.$prefix.$curType."-".$cpt."}\n";
	$cpt++;
	
	# We update the stats
	# uncomment below if curType is uncommented above
	#if ($ccode eq "(b)") {
	#    $pertype{'ACL(N)'}++;
	#}
	#elsif ($curCode eq "(d)") {
	#    $pertype{'ASCL(N)'}++;
	#} 
	#else {
	    $pertype{$curType}++;
	#}
    }
    elsif ($l =~ m/^\\bibauthor\{(.*)\}$/) {
	# To underline the authors from LIFO
	my $aut = $1;
	#print $aut;
	my @names  = split(/ and /,$aut);
	#print "*** ";
	#print @names;
	#print "\n";
	foreach my $name (@names) {
	    #print $name."  ";
	    # cleaning the extra whitespaces plus final punctuation
	    $name =~ s/^\s+//;
	    $name =~ s/\.?\s+%\s*$//;
	    # finding names (beware of compounds)
	    my @words = split(/ /,$name);
	    # we look for affiliated members using the last name (last token)
	    my $d = pop(@words);
	    # processing accents
	    $d =~ s/é/e/g; 
	    $d =~ s/â/a/g; 
	    $d =~ s/ê/e/g; 
	    $d =~ s/î/i/g; 
	    $d =~ s/ô/o/g; 
	    # in case the extraction used protected accents
	    $d =~ s/\{\\'e\}/e/g; 
	    $d =~ s/\{\\\^a\}/a/g; 
	    $d =~ s/\{\\\^e\}/e/g; 
	    $d =~ s/\{\\\^\\i\}/i/g; 
	    $d =~ s/\{\\\^o\}/o/g; 
	    #print "To find: ".uc($d)."\n";
	    my $tofind = uc($d);
	    #
	    my $count = grep / $tofind /, @allnames;
	    if ($count == 0) {
		#print "NOT FOUND: ".uc($d)."\n";
		print OUTFILE '\bibauthor{'.$name.'}'."\n";
	    } else {
		#print "FOUND : ".uc($d)."\n";
		print OUTFILE '\bibauthor*{'.$name.'}'."\n";
	    }
	}
    }
    elsif ($l =~ m/^\\begin\{entry\}/) {
	print OUTFILE "\n".$l;
    }
    elsif ($l =~ m/^\s*%\s*$/) {
	# To remove comments
    }
    elsif ($l =~ m/^\\bibmonth\{([^\}]*)\}/) {
	# for months
	my $mm = $1;
	my $month=" ";
	if ( defined($int2month{$mm}) ) {
	    $month = $int2month{$mm};
	}
	else {
	    $month = $mm;
	}
	print OUTFILE '\bibmonth{'.$month.'}'."\n";
    }
    elsif ($l =~ m/^\\bibyear\{([^\}]*)\}/) {
	# For stats per year
	my $zeyear = $1;
	if ( !(exists $peryear{$zeyear}) ) {
	    $peryear{$zeyear} = {};
	}
	$peryear{$zeyear}->{$curType}++;
	print OUTFILE $l;
    }
    else {
	print OUTFILE $l;
    }
}

if ( ($empty) ) {
    #print $sectionCpt."\n";
    my $j = $sectionCpt;
    for (; $j < 13 ; $j++) {
	print OUTFILE '\begin{lifo.bib.empty}{'.$code2name{$int2code{$j}}."}\n";
	print OUTFILE '\end{lifo.bib.empty}'."\n";
    }
}

close INFILE;
close OUTFILE;

# Post-processing, computing the statistics:
my $tot = 0;
print STATS "Publications per type\n";
print STATS "---------------------\n";
foreach my $type (@orderedtypes) {
    print STATS $type."\t : ".$pertype{$type}." publication(s)\n";
    $tot += $pertype{$type};
}
print STATS "\nTotal: ".$tot." publication(s)\n";
print STATS "\n";
$tot = 0;
print STATS "Publications per year\n";
print STATS "---------------------\n";
my @cles = sort (keys %peryear);
foreach my $y (@cles) {
    print STATS $y.": \n";
    foreach my $ty (@orderedtypes) {
	if (defined($peryear{$y}->{$ty})) {
	    print STATS "\t$ty\t: ".$peryear{$y}->{$ty}." publication(s)\n";
	    $tot += $peryear{$y}->{$ty};
	}
	else {
	    print STATS "\t$ty\t: 0 publication(s)\n";
	}
    }
}
print STATS "\nTotal: ".$tot." publication(s)\n";

close STATS;

# In the end, we remove the pre-file (i.e. the input file)
(unlink $file) or die("Error, cannot remove $out : $!");

exit 0;
