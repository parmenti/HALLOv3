#!/usr/bin/perl -w
#
# This file is part of HALLO.
#
# HALLO is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# HALLO is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with HALLO.  If not, see <http://www.gnu.org/licenses/>.
#
# Author: Y.Parmentier
# Date: 2014/11/21
# v. 3.0
#

use strict;
use warnings;
use File::Spec;
use File::Basename;
use Getopt::Std;
use POSIX qw/strftime/;

#####################################################
# FUNCTIONS:

sub main::HELP_MESSAGE()  
{
    print "Usage:\n  perl $0 -i input [-p prefix] [-e type_exceptions] [-n names] [-g field_exceptions] [-b|f] [-d] [-l] [-m MAX] [-c] [-w] [-s|r|x|y] [-j] [-u logins -a path] [-o others] [-t types] [-k countries] [-z output] \n\n";
    print "where:\n";
    print "input is \n   either a request file/repository containing queries such as \"year:2006 + author:toto\" \n";
    print "   or a BibTeX file/repository to format\n";
    print "type_exceptions is a file containing : hal-012345 ; ACL \n";
    print "field_exceptions is a file containing : hal-012345 ; year ; 2005 \n";
    print "prefix is a string \n";
    print "names is a file containing a name per line (for underlining)\n";
    print "-b: to create a BibTeX file ONLY \n";
    print "-f: to format a pre-computed BibTeX ONLY (no request sent to HAL)\n";
    print "-d: to decompose the accents into their latex code \n";
    print "-l: to create a log file \n";
    print "-m: to indicate the maximum number of retrieved references (enforced by HAL API, default value: 200) \n";
    print "-c: to add comments in the output BibTeX file \n";
    print "-w: to add empty section headers in the output latex file (whatever the output format is)\n";
    print "-s: to use a semantic formatting (instead of the syntactic default one)\n";
    print "-r: to use a rapport-style formatting (instead of the syntactic default one)\n";
    print "-x: to use a weblifo-style formatting (instead of the syntactic default one)\n";
    print "-y: to use a weblifo-en-style formatting (instead of the syntactic default one)\n";
    print "-u: single-user mode (the request is found automatically)\n";
    print "-a: where to find the .req files (-u needs -a)\n";
    print "-o: other types to be printed (convertion from AERES' types to new ones)\n";
    print "-t: extract only articles of given types (.type file is made of a type per line, type belonging to {UNDEFINED ART_ACL ART_SCL CONF_INV COMM_ACT COMM_SACT OUVS COVS DOUV PATENT OTHER REPORT THESE HDR})\n";
    print "\nNB: options -f and -b are not compatible, -r has priority over -s, -i has priority over -u.\n";
    print "-k: use a file containing the list of translations Fr-En of country names\n";
    print "-z output: define an output repo (optional, default: repo of the input file)\n";
    print "\n";
}

sub main::VERSION_MESSAGE()
{
    print "HALLO version 3.0 - November 2014\n";
}

sub get_timestamp 
{
    my ($auser) = @_;
    my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
    if ($mon < 10) { $mon = "0$mon"; }
    if ($hour < 10) { $hour = "0$hour"; }
    if ($min < 10) { $min = "0$min"; }
    if ($sec < 10) { $sec = "0$sec"; }
    $year=$year+1900;
    
    return $year . '_' . $mon . '_' . $mday . '__' . $hour . '_' . $min . '_' . $sec . '_' . $wday . '_' . $yday . '_' . $isdst . '_' . $auser;
}

sub update_prefs
{
    my ($rep, $which) = @_;
    my $preffile = File::Spec->catfile($rep,$which);
    if ("$^O" =~ /MSWin/) {
	# to write the path separator as / in the xml file
	$rep =~ s/\\/\//g;
    }
    my @relpref = ("perl",
		   "-i~", "-pe", "\"s#\Q./jabref-\E#$rep/jabref-#\"",
		   "\"$preffile\"");
    my $rel = join(" ",@relpref);
    system($rel);
}

sub clean_prefs
{
    my ($rep, $which) = @_;
    my $preffile = File::Spec->catfile($rep,$which);
    if ("$^O" =~ /MSWin32/) {
	$rep =~ s/\\/\//g;
    }
    my @relpref = ("perl",
		   "-i~", "-pe", "\"s#\Q$rep/jabref-\E#./jabref-#\"",
		   "\"$preffile\"");
    my $rel = join(" ",@relpref);
    system($rel);
}

sub processRequest
{
    #print "*** ";
    #print @_;
    my ($out,$afile, $prefix, $exceptions, $underline, $dexcep, $newcodes, $bibonly, $decode, $logg, $comments, $headers, $semrep, $form, $halids, $typ, $coun, $mnum) = @_;

    my @suffixes;
    push(@suffixes, '.txt');
    push(@suffixes, '.tex');
    push(@suffixes, '.pre');
    push(@suffixes, '.req');
    push(@suffixes, '.bib');
    push(@suffixes, '.hal');
    push(@suffixes, '.type');
    my ($name,$path) = fileparse($afile,@suffixes);

    if ($out ne " ") {
	$path = $out;  #should the user defined an output repository, we should use it instead of the input file's repo
    }

    if ( ($bibonly ne " ") && ($form ne " ") ) {
	print "Options -b and -f are not compatible.\n";
	print "Option -f is ignored.\n";
	$form = " ";
    }
    
    my $zeuser = $ENV{'USER'};
    if ( !defined($zeuser) ) {
	$zeuser = $ENV{'USERNAME'};
    }

    my $where = File::Spec->rel2abs(dirname($0));
    #print $where."\n";
 
    print "\nProcessing input file (.bib OR .req) \"$name\" ...\n\n";

    # 1. HAL extraction
    my @args = ();
    my $cmd = "";
    my $hal_search = File::Spec->catfile($where,"hal_search.pl");
    if ($form eq " ") {
	@args = ("perl", 
		 "\"$hal_search\"", 
		 "-i", "\"$afile\"", $decode, $logg, $comments);
	
	if ($exceptions ne " ") {
	    push(@args, "-e", "\"$exceptions\"");
	}
	if ($dexcep ne " ") {
	    push(@args, "-g", "\"$dexcep\"");
	}
	if ($halids ne " ") {
	    push(@args, "-j");
	}
	if ($typ ne " ") {
	       push(@args, "-t", "\"$typ\"");
	}
	if ($coun ne " ") {
	       push(@args, "-k", "\"$coun\"");
	}
	if ($out ne " ") {
	       push(@args, "-z", "\"$out\"");
	}
	if ($mnum ne " ") {
	       push(@args, "-m", "\"$mnum\"");
	}
	
        #print @args;
	$cmd = join(" ",@args);
	if ( system($cmd) == 0 ) {
	    #print "\n... created bibtex file \"".$path.$name.".bib\"\n";
	    print "\n... created bibtex file \"".$name.".bib\"\n";
	}
	else {
	    #die "system @args failed: $?";
	    die "An error occurred (hal search) : $?\n";
	}
    }
    # 2. Bibtex reformatting
    if ($bibonly eq " ") {
        # We temporarily redirect stderr and stdout as JabRef is too verbose
        my $tmpout = File::Spec->catfile(
            File::Spec->catfile($where,'temp'), "log.tmp.out-".get_timestamp($zeuser)
            );
        my $tmperr = File::Spec->catfile(
            File::Spec->catfile($where,'temp'), "log.tmp.err-".get_timestamp($zeuser)
            );
        # a.copy of the file descriptors
        open(CPOUT, ">&STDOUT");
        open(CPERR, ">&STDERR");
        # b. redirect stdout + stderr in to warning file
        open (STDOUT, ">$tmpout") or die ("Can't write to $tmpout: $!");
        open (STDERR, ">$tmperr") or die ("Can't write to $tmperr: $!");

        # c. we build the correct command
        my $jabref= File::Spec->catfile($where,"JabRef-2.5.jar");
        my $pref = File::Spec->catfile($where,"jpref.xml");
        my $outp = File::Spec->catfile($path,$name.".pre,haltex");
        my $outb = File::Spec->catfile($path,$name.".bib");
        @args = ("java", 
        	 "-jar", "\"$jabref\"", "-n",
        	 "-p", "\"$pref\"",
        	 "-o", "\"$outp\"", "\"$outb\"");
        $cmd = join(" ",@args);
        #print $cmd."\n";
        if ($semrep eq " ") {
            # To make the preference file tolerent to call from relative path
            update_prefs($where, "jpref.xml");
        }
        elsif ($semrep eq "-s") {
            $cmd =~ s/jpref\.xml/jprefsem\.xml/;
            $cmd =~ s/,haltex/,lifotex/;
            update_prefs($where, "jprefsem.xml");
        }
        elsif ($semrep eq "-r") {
            $cmd =~ s/jpref\.xml/jprefrap\.xml/;
            $cmd =~ s/,haltex/,rapport/;
            update_prefs($where, "jprefrap.xml");
        }
        elsif ($semrep eq "-x") {
            $cmd =~ s/jpref\.xml/jprefweb\.xml/;
            $cmd =~ s/pre,haltex/html,weblifo/;
            update_prefs($where, "jprefweb.xml");
        }
        elsif ($semrep eq "-y") {
            $cmd =~ s/jpref\.xml/jprefweben\.xml/;
            $cmd =~ s/pre,haltex/html,weblifo-en/;
            update_prefs($where, "jprefweben.xml");
        }

        system($cmd); #we run it twice cf bug when prefs changed in JabRef
        my $result = system($cmd); 

        # we clean the pref file
        if ($semrep eq " ") {
            clean_prefs($where, "jpref.xml");
        }
        elsif ($semrep eq "-s") {
            clean_prefs($where, "jprefsem.xml");
        }
        elsif ($semrep eq "-r") {
            clean_prefs($where, "jprefrap.xml");
        }
        elsif ($semrep eq "-x") {
            clean_prefs($where, "jprefweb.xml");
        }
        elsif ($semrep eq "-y") {
            clean_prefs($where, "jprefweben.xml");
        }

        # d. close the redirected filehandles
        close(STDOUT) or die ("Can't close STDOUT: $!");
        close(STDERR) or die ("Can't close STDERR: $!");
        # e. restore stdout + stderr
        open(STDOUT, ">&CPOUT") || die "Can't restore STDOUT: $!";
        close(CPOUT);
        open(STDERR, ">&CPERR") || die "Can't restore STDERR: $!";
        close(CPERR);
        if (-e "$tmpout") {
            (unlink "$tmpout") or die("Can't remove $tmpout : $!");
        }
        if (-e "$tmperr") {
            (unlink "$tmperr") or die("Can't remove $tmperr : $!");
        }
        # f. back to normal
        if ( $result == 0 ) {
            #print "\n... created pre-tex file \"".$path.$name.".pre\"\n";    
            print "\n... created pre-tex/html file \"".$name.".(pre|html)\"\n";    
        }
        else {
            #die "system @args failed: $?";
            die "An error occurred (JabRef call) : $?\n";
        }
        # 3. Numbering of the entries and computing of statistics
        if ( ($semrep ne "-x") && ($semrep ne "-y") ) {
            @args = ();
            if ($underline eq " ") {
        	$underline = File::Spec->catfile(
        	    $where, 'data', 'names.txt'
        	    );
            }
            my $pref="";
            if ($prefix ne " ") {
        	$pref = "-p ".$prefix;
            }
            my $ncodes="";
            if ($newcodes ne " ") {
        	$ncodes = "-o ".$newcodes;
            }
            my $numbering = File::Spec->catfile($where,"numbering4tex.pl");
            my $prefile   = File::Spec->catfile($path,$name.".pre");
            @args = ("perl", 
        	     "\"$numbering\"", 
        	     "-i", "\"$prefile\"", 
        	     "-n", "\"$underline\"", $pref, $headers, $decode, $ncodes);
            $cmd = join(" ",@args);
            if ($semrep eq "-s") {
        	$cmd =~ s/numbering4tex\.pl/numbering4lifo\.pl/;
            }
            elsif ($semrep eq "-r") {
        	$cmd =~ s/numbering4tex\.pl/numbering4rapport\.pl/;
            }
            if ( system($cmd) == 0 ) {
        	#print "\n... created tex file \"".$path.$name.".tex\"\n";
        	print "\n... created tex file \"".$name.".tex\"\n";
            }
            else {
        	#die "system $cmd failed: $?";
        	die "An error occurred (numbering) : $?\n";
            }
        }
    }
}

sub getRequest
{
    my ($login, $lname) = @_;
    my %logins = ();

    open(LOGIN, "<$login") or die("Error while opening $login : $!");
    my @alllogins = <LOGIN>;
    #print @alllogins;
    close(LOGIN);
    foreach my $lo (@alllogins) {
	if ($lo =~ m/^([A-Za-z]+)\s+;(.+)$/) {
	    my $log = $1;
	    my $nam = $2;
	    $nam =~ s/\s+$//g;
	    $nam =~ s/^\s+//g;
	    $nam =~ s/ /_/g;
	    #print "$log - $nam\n";
	    $logins{$log} = $nam;
	}
    }
    
    my $res = " ";
    if ( defined($logins{$lname}) ) {
	$res = $logins{$lname}.".req";
    }
    return $res;
}
#
################################################

my $time = strftime('%d-%b-%Y %H:%M',localtime); 

$Getopt::Std::STANDARD_HELP_VERSION = 1;

my %opts;
getopts('i:p:e:dblcn:wsrxyfu:a:g:o:jt:k:z:m:', \%opts);
# i input request / BibTeX rep/file
# p prefix
# e exceptions
# n names (for underlining)
# d with accent decoding
# b bibonly
# l logging on
# m max number redefined
# c comments on
# w with (empty section)'s headers 
# s semantic format
# r rapport format
# x weblifo format
# y weblifo-en format
# f formatting only
# u single user (needs to know the logins)
# a path where to find req files (for single user mode only)
# g date exceptions
# o other types
# j hal ids as an input
# t types
# k countries
# z output repo

my @reqfile = ();
my $user  = $ENV{"USER"};
if ( !defined($user) ) {
    $user = $ENV{'USERNAME'};
}

if ( (exists $opts{'u'}) ) {
    if ( (exists $opts{'a'}) ) {
	my $lfile = $opts{'u'};
	#print $lfile." ".$user."\n";
	my $request = getRequest($lfile, $user);
	if ($request eq " ") {
	    print STDERR "** Error: Unknown user ($user). **\n";
	    exit 3;
	}
	else {
	    $request = File::Spec->catfile($opts{'a'},$request);
	    if ( !(-e "$request") ) {
		#print $request."\n";
		print STDERR "** Error: input file does not exist. **\n";
		exit 4;
	    }
	    else {
		push(@reqfile, $request);
	    }
	}
    }
    else {
	print STDERR "** Error: Options -u and -a have to be set conjoinly. **\n";
	main::HELP_MESSAGE();
	exit 2;
    }
}
elsif ( !(exists $opts{'i'}) ) {
    print STDERR "** Error: input file / directory is missing. **\n";
    main::HELP_MESSAGE();
    exit 1;
}

my $pref = " ";
my $exc  = " ";
my $dexc = " ";
my $dec  = " ";
my $bibo = " ";
my $log  = " ";
my $com  = " ";
my $und  = " ";
my $with = " ";
my $sem  = " ";
my $format=" ";
my $file = " ";
my $ocodes=" ";
my $in_ids=" ";
my $types =" ";
my $cou   =" ";
my $outrep=" ";
my $maxnum=" ";

if ( exists($opts{'p'}) ) {
    $pref = $opts{'p'};
}
if ( exists($opts{'e'}) ) {
    $exc  = $opts{'e'};
}
if ( exists($opts{'n'}) ) {
    $und  = $opts{'n'};
}
if ( exists($opts{'g'}) ) {
    $dexc = $opts{'g'};
}
if ( exists($opts{'o'}) ) {
    $ocodes= $opts{'o'};
}
if ( exists($opts{'m'}) ) {
    $maxnum = $opts{'m'};
}
if ( defined($opts{'b'}) ) {
    $bibo = "-b";
}
if ( defined($opts{'d'}) ) {
    $dec  = "-d";
}
if ( defined($opts{'l'}) ) {
    $log  = "-l";
}
if ( defined($opts{'c'}) ) {
    $com  = "-c";
}
if ( defined($opts{'w'}) ) {
    $with = "-w";
}
if ( defined($opts{'s'}) ) {
    $sem = "-s";
}
if ( defined($opts{'r'}) ) {
    $sem = "-r";
}
if ( defined($opts{'x'}) ) {
    $sem = "-x";
}
if ( defined($opts{'y'}) ) {
    $sem = "-y";
}
if ( defined($opts{'f'}) ) {
    $format = "-f";
}
if ( defined($opts{'j'}) ) {
    $in_ids = "-j";
}
if ( exists($opts{'i'}) ) {
    push(@reqfile,$opts{'i'});
}

my $secret = " "; 
if ( exists $opts{'u'} ) {
    $secret = "-u";
}
if ( exists $opts{'t'} ) {
    $types = $opts{'t'};
}
if ( exists $opts{'k'} ) {
    $cou = $opts{'k'};
}
if ( exists $opts{'z'} ) {
    $outrep = $opts{'z'};
}

# we set the umask rights (if we're not under windows)
if ( ("$^O" !~ /MSWin/) ) {
    #print "umask: ".umask()."\n";
    if ($secret eq "-u") {
	# In single user mode, the output files are not readable for anybody 
	umask(54);
    } 
    #else {
	# otherwise, the output files are editable by the group
	#umask(6);
    #}
    #print "** umask: ".umask()."\n";
}

foreach my $file (@reqfile) {

    #print "Arguments: $file, $pref, $exc, $und, $bibo, $dec, $log, $com, $with, $sem, $format, $inbib\n";
    
    # If $file is a directory
    if ( -d $file ) {
	opendir (DIR, $file) or die("Error while opening directory $file : $!\n");
	
	while ( my $filename = readdir(DIR) )
	{
	    if ($filename =~ /.*\.req$/) {
		#print "$filename\n";
		my $zefile = File::Spec->catfile($file,$filename);
		#print "*** ($outrep, $zefile, $pref, $exc, $und, $dexc, $ocodes, $bibo, $dec, $log, $com, $with, $sem, $format, $in_ids, $types, $cou, $maxnum)\n";
		processRequest($outrep, $zefile, $pref, $exc, $und, $dexc, $ocodes, $bibo, $dec, $log, $com, $with, $sem, $format, $in_ids, $types, $cou, $maxnum);
	    }
	}
	
	close(DIR);
    }
    # otherwise
    else {
	#print "*** ($outrep, $file, $pref, $exc, $und, $dexc, $ocodes, $bibo, $dec, $log, $com, $with, $sem, $format, $in_ids, $types, $cou, $maxnum)\n";
	processRequest($outrep,$file, $pref, $exc, $und, $dexc, $ocodes, $bibo, $dec, $log, $com, $with, $sem, $format, $in_ids, $types, $cou, $maxnum);
    }
}

exit 0;
