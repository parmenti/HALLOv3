#!/usr/bin/perl -w
#
# This file is part of HALLO.
#
# HALLO is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# HALLO is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with HALLO.  If not, see <http://www.gnu.org/licenses/>.
#
#
# Author: Y.Parmentier
# Date: 2014/11/21
# v. 3.0
#

#use strict;
use warnings;
use Getopt::Std;
use File::Spec;
use File::Basename; #should be part of Perl's core
use Encode;
#use WWW::Curl::Easy; #more configurable alternative to LWP::Curl
use LWP 5.64; # Loads all important LWP classes, and makes sure your version is recent
use JSON;  
use IO::File;
use POSIX qw/strftime/;
use Data::Dumper; #for debugging
use LaTeX::Encode ':all', remove => [ '$' ] , add => { '~' => '' , '’' => '\''};

##############################################################################
# DEFINITIONS OF FUNCTIONS:
#
sub main::HELP_MESSAGE()  
{
    print "Usage:\n  perl hal_search.pl -i input_request [-e exceptions] [-d] [-l] [- c] [-j] [-g field_exceptions]\n [-t types] [-k countries] [-m MAX]\n";
    print "where:\nrequest file containing queries such as \"year:2006 + author:toto ...\"\n";
    print "exceptions file containing : hal-012345 ; ACL \n";
    print "-d: to decompose the accents into their latex code \n";
    print "-l: to create a log file \n";
    print "-c: to add comments into the bibtex \n";
    print "field_exceptions file containing : hal-012345 ; year ; 2005 \n";
    print "-t: use a file containing the list of types to consider";
    print "-k: use a file containing the list of translations Fr-En of country names";
    print "-z: define an output repo (instead of the input file's repo)";
    print "-m: indicate the maximum number of retrieved references (enforced by HAL API, default value: 200) \n";
    print "\n";
}

sub main::VERSION_MESSAGE()
{
    print "hal_search.pl version 3.0 - November 2014\n";
}

sub check_field
{
    my ($bad, $pubid, $field_name, $default) = @_;
    my $res = "";
    #print $pubid. " " . $field_name . " " . $default . "\n";

    # we change the field if there is an exception
    if ( exists($bad->{$pubid}) ) {
	#print $pubid. " is AN EXCEPTION ($bad->{$pubid})\n";
	my $href = $bad->{$pubid};
	if ( exists($href->{$field_name}) ) {
	    #print $pubid. " is AN EXCEPTION (${$bad->{$pubid}}{$field_name})\n";
	    $res = $href->{$field_name};
	}
	else {
	    $res = $default;
	}
    }
    else {
	$res = $default;
    }
    return $res;
}

sub add_field
{
   my ($bad, $pubid, $dejavu) = @_;
   my $res = "";

   if ( exists($bad->{$pubid}) ) {
       foreach my $key (keys(%{$bad->{$pubid}})) {
	   #print $pubid . " ** " . $key . "\n";
	   my @match = grep(/^$key$/,@{$dejavu});
	   my $sizematch = @match;
	   if ($sizematch == 0) {
	       #print $pubid . " ** " . $key . "\n";
	       $res .= $key . " = {" . ${$bad->{$pubid}}{$key} ."},\n";
	   }
       }
   }
   return $res;
}


sub process_entry
{
    my ($mem, $enco, $anEntry, $allTypeOutlaws, $allOutlaws, $allCountrys, $halTOaeres, $aeresTOcode, $allmeta,$log, $logfile, $intTOmonth) = @_;
    my $bibres = "";
    
    if ($log) {
        print $logfile Dumper($anEntry); ##For debugging only
        #print Dumper($allOutlaws);
    }

    my $zid = $anEntry->{'halId_s'};

    #print("******** ". $zid."\n");

    if( !exists( $mem->{$zid} ) )
    {
        # We store the id to avoid storing it twice
        $mem->{$zid} = 1;
        
        # We prepare the pieces of information that 
        # are needed for building the bibtex entry
        my $v = $anEntry->{'version_i'};
        my $typeHAL="";
        my $author="";
        my $title=$anEntry->{'title_s'}[0];
        my $journal="";
        my $month="";
        my $year="";
        my $dateconf="";
        my $datewrite="";
        my $volume="";
        my $number="";
        my $pages="";
        my $pagescomment = ""; #for cases where page numbers are entered in the comment field
        my $doi="";
        my $url="https://hal.science/".$zid;
        my $abstract = "";
        my $publisher="";
        my $audience="";
        my $booktitle="";
        my $titouv="";
        my $titconf="";
        my $school="";
        my $ville="";
        my $pays="";
        my $comment="";
        my $serie="";
        my $edsci="";
        my $datephd="";
        my $reviewed="";
        my $withProc="";
        my $desc="";
	my $confinv="";
        my @knowns = ();
        #title_s and authors and issue_s and abstract_s are processed separately cause they're lists

        if ( defined ($anEntry->{'scientificEditor_s'}) ){
            my @zeEds = @{$anEntry->{'scientificEditor_s'}};
            foreach my $zI (0 .. $#zeEds) {
                $edsci .= $zeEds[$zI];
                if ($zI < $#zeEds) {
                    $edsci .= " and ";
                }
            }
	    $edsci =~ s/([^\\])"/$1''/g; 
            if ($enco) {
                $edsci = latex_encode($edsci, {});
            }
        }
        
        if ( defined ($anEntry->{'issue_s'}) ) {
            $number = $anEntry->{'issue_s'}[0];
            if ($enco) {
                $number = latex_encode($number, {});
            }
            push(@knowns,"number");
        }

        if ( defined ($anEntry->{'abstract_s'}) ) {
	    $abstract = $anEntry->{'abstract_s'}[0];
	    $abstract =~ s/([^\\])"/$1''/g; 
	    $abstract =~ s/\\//g;
            if ($enco) {
                $abstract = latex_encode($abstract, {});
            }
            push(@knowns,"abstract");
        }

        # Processing of the authors names
        my @authorsL = @{$anEntry->{'authLastName_s'}};
        my @authorsF = @{$anEntry->{'authFirstName_s'}};
        foreach my $i (0 .. $#authorsL) {
            my $fname = $authorsF[$i];
            my $lname = $authorsL[$i];
            $author .= $lname.", ".$fname;
            if ($i < $#authorsL) {
                $author .= " and ";
            }
        }
        if ($enco) {
            $author = latex_encode($author, {});
        }
        #Latex Title with protected uppercase letters
	$title =~ s/([^\\])"/$1''/g; 
        if ($enco) {
            $title = latex_encode($title, {});
        }
        $title =~ s/([A-Z])/\{$1\}/g; ## to do only after latex encoding!

        # PhD School
        if ( defined ($anEntry->{'authorityInstitution_s'}) ) {
            $school = $anEntry->{'authorityInstitution_s'}[0];
            $school =~ s/([^\\])"/$1''/g; 
            if ($enco) {
                $school = latex_encode($school);
            }
            push(@knowns,"school");
        }

        #Publisher
        if ( defined ($anEntry->{'publisher_s'}) ) {
            $publisher = $anEntry->{'publisher_s'}[0];
            $publisher =~ s/([^\\])"/$1''/g; 
            if ($enco) {
                $publisher = latex_encode($publisher);
            }
            push(@knowns,"publisher");
        }
        
        #DOI (no latex encoding here)
        if ( defined ($anEntry->{'doiId_s'}) ) {
            $doi = $anEntry->{'doiId_s'};
        }

        #All other fields
        foreach my $zekey (@{$allmeta}) {
            
            my $zeval = $anEntry->{$zekey}; 

            if ( defined $zeval) {
                #print "*** DEBUG: $zekey -- $zeval \n";

                # We check the field
                $zeval = check_field($allOutlaws,$zid,$zekey,$zeval);
                #print "DEBUG: $enco -- $zeval\n";
                
                # We store the processed field, to add the new ones from %doutlaws
                push(@knowns,$zekey); 
                push(@knowns,"page");
                push(@knowns,"year");

                # Must be done prior to latex encoding
                # to replace " with '' when not used in a command such as {\"o}
                # this is done because of a conflict between babel's commands 
                # for french and russian options:
                $zeval =~ s/([^\\])"/$1''/g; 
                # Regexp for parsing JR's and VL's papers
                $zeval =~ s/\x{03b2}/\$\\beta\$/g; 
                $zeval =~ s/\x{25e6}/\$\\circ\$/g; 
                $zeval =~ s/\x{fb01}/fi/g; 
                $zeval =~ s/\x{201c}/''/g; 
                $zeval =~ s/\x{201d}/''/g;
		$zeval =~ s/\x{2019}/\'/g;
		
                ## Must be done prior to processing URLs:
                if ($enco) {
                    $zeval = latex_encode($zeval, {});
                }
                # Regexp for parsing URL's
                if ($enco) {
                    $zeval =~ s/\s+((https?|ftps?):\/\/[^:\/]+(:\d+)?.*[^\.,])/ \\url{$1}/g;
                }               
                # extra latex encodings:
                $zeval =~ s/:([^\/])/{:}$1/g; 
                $zeval =~ s/@/ AT /g; 

                ## Key,Value processing
                if ($zekey eq "docType_s") {
                    $typeHAL = $zeval;
		    #print $typeHAL."\n";
                }
                elsif ($zekey eq "conferenceStartDateY_i") {
                    $year = $zeval;
                }
                elsif ($zekey eq "page_s") {
                    $pages = $zeval;
                }
                elsif ($zekey eq "producedDate_s") {
                    $datephd = $zeval;
                }
                elsif ($zekey eq "journalTitle_s") {
                    $journal = $zeval;
                }
                elsif ($zekey eq "audience_s") {
                    $audience = $zeval;
                }
                elsif ($zekey eq "proceedings_s") {
                    $withProc = $zeval;
                }
                elsif ($zekey eq "serie_s") {
                    $serie = ${zeval}->[0];
                }
                elsif ($zekey eq "volume_s") {
                    $volume = $zeval;
                }
                elsif ($zekey eq "conferenceTitle_s") {
                    $titconf = $zeval;
                }
                elsif ($zekey eq "bookTitle_s") {
                    $titouv = $zeval;
                }
                elsif ($zekey eq "city_s") {
                    $ville = $zeval;
                }
                elsif ($zekey eq "country_s") {
                    my $cle = uc($zeval);
                    if (exists($allCountrys->{$cle})) {
                        $pays = $allCountrys->{$cle};
                    } else {
                        $pays = $zeval;
                    }
                }
                elsif ($zekey eq "conferenceStartDateY_i") {
                    $dateconf = $zeval;
                }
                elsif ($zekey eq "producedDate_s") {
                    $datewrite = $zeval;
                }
                elsif ( $zekey eq "journalPublisher_s" ) { 
                ## can only have one of publisher_s and journalPublisher_s
                    $publisher = $zeval;
                }
                elsif ($zekey eq "comment_s") {
                    $comment = $zeval;
                }
                elsif ($zekey eq "conferenceStartDateM_i") {
                    if ( $zeval ne "0") {
                        if ($zeval < 10) 
                        {$month = "0".$zeval;}
                        else
                        {$month = $zeval;}
                    }
                }
                elsif ($zekey eq "peerReviewing_s") {
                    $reviewed = $zeval;
                }
                elsif ($zekey eq "description_s") {
                    ## for extra comments (e.g. JIRC, etc.)
                    $comment = $zeval.", ".$comment; 
                }
		elsif ($zekey eq "invitedCommunication_s") {
		    ## for invited conferences
		    $confinv = $zeval;
		}
            }
        }
        
        # Processing of the date
        my $zeyear = "";
        if ( exists($allOutlaws->{$zid}->{year}) ) {
            $zeyear = $allOutlaws->{$zid}->{year};
        }
        elsif ($year ne "") {
            $zeyear = substr($year,0,4);
            # month 
            if ( length($year) > 5) {
                $month = substr($year, 5,2);
            }
        }
        elsif ($dateconf ne "") {
            $zeyear = substr($dateconf,0,4);
            # month 
            if ( length($dateconf) > 5) {
                $month = substr($dateconf, 5,2);
            }
        }
        elsif ($datephd ne "") {
            $zeyear = substr($datephd,0,4);
            # month 
            if ( length($datephd) > 5 ) {
                $month = substr($datephd, 5,2);
            }
        }
        elsif ($datewrite ne "") {
            $zeyear = substr($datewrite,0,4);
            # month 
            if ( length($datewrite) > 5 ) {
                $month = substr($datewrite, 5,2);
            }
        }
        else {
            $zeyear = "To appear";
        }
        
        # Processing of the booktitle
        if ( ($titouv ne "") && ($titconf ne "") ) {
            if ($titouv ne $titconf) {
                $booktitle = $titconf.', '.$titouv;
            }
            else {
                $booktitle = $titconf;
            }
        }
        elsif ($titouv ne "") {
            $booktitle = $titouv;
        }
        elsif ($titconf ne "") {
            $booktitle = $titconf;
        }
        #print "***DEBUG: $booktitle \n";	    
        
        # Finally we write the bibtex entry, depending on the bibtex type
        
        # First, the typical mandatory field
        if ($typeHAL eq 'ART') {
            $bibres .= '@ARTICLE{'.$zid.":".$v.",\n\t";
            $bibres .= "journal = {".$journal."},\n\t";
            $bibres .= "author = {".$author."},\n\t";
        }
        elsif ($typeHAL eq 'PRESCONF'){
            $bibres .= '@INPROCEEDINGS{'.$zid.":".$v.",\n\t";
            $bibres .= "booktitle = {".$booktitle."},\n\t";
            $bibres .= "author = {".$author."},\n\t";
        }
        elsif ($typeHAL eq 'COMM'){
            $bibres .= '@INPROCEEDINGS{'.$zid.":".$v.",\n\t";
            $bibres .= "booktitle = {".$booktitle."},\n\t";
            $bibres .= "author = {".$author."},\n\t";
        }
        elsif ($typeHAL eq 'POSTER'){
            $bibres .= '@INPROCEEDINGS{'.$zid.":".$v.",\n\t";
            $bibres .= "booktitle = {".$booktitle."},\n\t";
            $bibres .= "author = {".$author."},\n\t";
        }
        elsif ($typeHAL eq 'OUV') {
            $bibres .= '@BOOK{'.$zid.":".$v.",\n\t";
            $bibres .= "publisher = {".$publisher."},\n\t";
        }
        elsif ($typeHAL eq 'COUV') {
            $bibres .= '@INCOLLECTION{'.$zid.":".$v.",\n\t";
            $bibres .= "booktitle = {".$booktitle."},\n\t";		
            $bibres .= "publisher = {".$publisher."},\n\t";
            $bibres .= "author = {".$author."},\n\t";
        }
        elsif ($typeHAL eq 'PROCEEDINGS') {
            $bibres .= '@PROCEEDINGS{'.$zid.":".$v.",\n\t";
            #$bibres .= "author = {".$author."},\n\t"; #NO AUTHOR for type BOOKS
            if ($edsci eq "") {
                $bibres .= "editor = {".$author."},\n\t"; #AUTHOR becomes EDITOR for @BOOK when no eds are given
            }
        }
        elsif ($typeHAL eq 'PATENT') {
            $bibres .= '@TECHREPORT{'.$zid.":".$v.",\n\t";
            $bibres .= "author = {".$author."},\n\t";
	    $bibres .= "institution = {".$school."},\n\t";	    	    
        }
        elsif ($typeHAL eq 'OTHER') {
	    $bibres .= '@MISC{'.$zid.":".$v.",\n\t";
	    $bibres .= "author = {".$author."},\n\t";
	    if ( ($comment =~ /\bUniversit/i) ) { ## For HDR which are not in TEL (i.e. they are simple HAL references)
		$bibres .= "note = {".$comment."},\n\t";
		$comment = ""; ## reset
	    }
        }
        elsif ($typeHAL eq 'REPORT') {
            $bibres .= '@TECHREPORT{'.$zid.":".$v.",\n\t";
            $bibres .= "author = {".$author."},\n\t";
	    $bibres .= "institution = {".$school."},\n\t";  
        }
        elsif ($typeHAL eq 'THESE') {
            $bibres .= '@PHDTHESIS{'.$zid.":".$v.",\n\t";
            $bibres .= "school = {".$school."},\n\t";
            $bibres .= "author = {".$author."},\n\t";
            if ( $enco ) {
                $bibres .= "note = {{T}h{\\`e}se de {D}octorat},\n\t";
            }
            else {
                $bibres .= "note = {{T}h{\\`e}se de {D}octorat},\n\t";
            }
        }
        elsif ($typeHAL eq 'HDR') {
            $bibres .= '@MISC{'.$zid.":".$v.",\n\t";
	    $bibres .= "note = {".$school.". {H}abilitation à {D}iriger des {R}echerches},\n\t";
            $bibres .= "author = {".$author."},\n\t";
        }
        elsif ($typeHAL eq 'MEM') {
            $bibres .= '@MASTERSTHESIS{'.$zid.":".$v.",\n\t";
            $bibres .= "school = {".$school."},\n\t";
	    $bibres .= "note = {{T}h{\\`e}se de {M}aster},\n\t";
            $bibres .= "author = {".$author."},\n\t";
        }
        elsif ($typeHAL eq 'UNDEFINED') {
            $bibres .= '@UNPUBLISHED{'.$zid.":".$v.",\n\t";
            $bibres .= "author = {".$author."},\n\t";
        }      
        elsif ($typeHAL eq 'IMG') {
            $bibres .= '@UNPUBLISHED{'.$zid.":".$v.",\n\t";
            $bibres .= "author = {".$author."},\n\t";
        }
        elsif ($typeHAL eq 'LECTURE') {
            $bibres .= '@UNPUBLISHED{'.$zid.":".$v.",\n\t";
            $bibres .= "author = {".$author."},\n\t";
        }
        
        # Second, the general mandatory field
        $bibres .= "title = {".$title."},\n\t";
        $bibres .= "url = {".$url."},\n\t";
        
        # AERES specific
        my $aeres = "";
	# for invited conferences
	if ($typeHAL eq "COMM" && $confinv eq "1") {# 1 is invited talk
	    $aeres = "INV";
	}
        # for national conferences
        #DEBUG 
        #print $title."\n";
        #print "###".$typeHAL.":".$audience."\n";
        elsif ($typeHAL eq "COMM" && $audience eq "3") {# 3 is national
            if ($reviewed eq "0" || $withProc eq "0") { #0 is not reviewed, 0 has no proceedings
                $aeres = "COM"; 
            } 
            else {
                $aeres = "ACTN";
            }   
        }
        elsif ($typeHAL eq "COMM" && $audience eq "2") { #2 is international
            if ($reviewed eq "0" || $withProc eq "0") {
                $aeres = "COM"; 
            } 
            else {
                $aeres = "ACTI";
            }
        }
        # for journals
        elsif ($typeHAL eq "ART" && $audience eq "3") {
            if ($reviewed eq "0") {
                $aeres = "ASCL(N)"; 
            } 
            else {
                $aeres = "ACL(N)";
            }
        }
        elsif ($typeHAL eq "ART" && $audience eq "2") {
            if ($reviewed eq "0") {
                $aeres = "ASCL"; 
            } 
	    else {
		$aeres = $halTOaeres->{$typeHAL};
	    }
        }
        else {
            $aeres = $halTOaeres->{$typeHAL};
        }
        if ( ($comment =~ /\bposter\b/i) ) {
            $aeres = "AFF";
        }
        if ( ($comment =~ /\bdemo\b/i) ) {
            $aeres = "AFF";
        }
        # we change the type if it is an outlaw
        if ( exists($allTypeOutlaws->{$zid}) ) {
            #print $zid. " is AN OUTLAW ($allTypeOutlaws->{$zid})\n";
            $aeres = $allTypeOutlaws->{$zid};
        }
        #print "***DEBUG: $aeres \n";
        ## code (for sorting)
        if ( defined($aeres) && exists($aeresTOcode->{$aeres}) ) {
            $bibres .= "code ={".$aeresTOcode->{$aeres}."},\n\t";
        } else {
            $aeres = " ";
            print "[WARNING] - unknown code for paper $zid ($typeHAL--$aeres)\n";
        }
        ##
        # To respect the AERES' types, once the codes have been computed
        $bibres .= "aeres = {".$aeres."},\n\t";
	$bibres .= "keywords = {".$aeres."},\n\t"; #for sorting using biblatex
	
        # Third, the optional fields
        if ($ville ne "" && $pays ne "") {
            $bibres .= "address = {".$ville.", ".$pays."},\n\t";
        }
        if ($publisher ne "" && $typeHAL ne "OUV" && $typeHAL ne "COUV") {
            $bibres .= "publisher = {".$publisher."},\n\t";
        }
        if ($abstract ne "") {
            $bibres .= "abstract = {".$abstract."},\n\t";
        }
        if ($volume ne "") {
            $bibres .= "volume = {".$volume."},\n\t";
        }
        if ($serie ne "") {
            $bibres .= "series = {".$serie."},\n\t";
        }
        if ($number ne "") {
            $bibres .= "number = {".$number."},\n\t";
        }
        if ($doi ne "") {
            $bibres .= "doi = {".$doi."},\n\t";
        }
        if ($month ne "") {
            #$bibres .= "month = {".$intTOmonth->{$month}."},\n\t";
	    $bibres .= "month = {".$month."},\n\t";
        }
        if ($edsci ne "") {
            $bibres .= "editor = {".$edsci."},\n\t";
        }
        if ($comment ne "") {
            if ($comment =~ m/[0-9]+\s*pages/) {
                $pagescomment = $comment;
                $pagescomment =~ s/^[^0-9]*([0-9]+)\s+pages.*$/$1/; 
                $comment =~ s/(^[^0-9]*)[0-9]+\s+pages[^a-zA-Z]*([a-zA-Z]*.*)$/$1$2/; 
            }
        }
        if ($comment ne "") {
            $bibres .= "comment = {".$comment."},\n\t";
        }
        if ($pages ne "" && $pages !~ "\\_") {
            $bibres .= "pages = {".$pages."},\n\t";
        }
        elsif ($pagescomment ne "") {
            $bibres .= "pages = {".$pagescomment."},\n\t";
        }
        # Unknowns fields to be added (cf %doutlaws)
        $bibres .= add_field($allOutlaws, $zid, \@knowns);
        # Last, the year
        $bibres .= "year = {".$zeyear."}\n}\n\n";
    }
    
    return $bibres;
}
#
#
##############################################################################


##############################################################################
# INITIALISATION
##############################################################################

my $time = strftime('%d-%b-%Y %H:%M',localtime); 

$Getopt::Std::STANDARD_HELP_VERSION = 1;

my %opts;
getopts('i:e:dlcg:t:k:z:m:', \%opts);
# i input rep/fil
# e exceptions
# d with accent decoding
# l with log
# c with comments
# g date exceptions
# t types
# k countries
# z output repo
# m MAX

if ( !(exists $opts{'i'}) ) {
    print STDERR "** Error: input file is missing. **\n";
    main::HELP_MESSAGE();
    exit 1;
}

# latex encoding of diacritics
my $enc = 0; #false by default
# logging
my $wlog= 0; #false by default
my $zelog; #filehandler
# comments
my $comments=0; #false by default
# exceptions file
my $except = "";
# date exceptions file
my $dexcept= "";
# types
my $types  = "";
# countries
my $countries = "";
# out repo
my $outrep = "";
# max refs
my $maxnum = "";

if ( defined($opts{'d'}) ) {
    $enc = 1; #true
}
if ( defined($opts{'l'}) ) {
    $wlog= 1; #true
}
if ( defined($opts{'c'}) ) {
    $comments= 1; #true
}
if ( exists($opts{'e'}) ) {
    $except = $opts{'e'};
}
if ( exists($opts{'g'}) ) {
    $dexcept = $opts{'g'};
}
if ( exists($opts{'t'}) ) {
    $types = $opts{'t'};
}
if ( exists($opts{'k'}) ) {
    $countries = $opts{'k'};
}
if ( exists($opts{'z'}) ) {
    $outrep = $opts{'z'};
}
if ( exists($opts{'m'}) ) {
    $maxnum = $opts{'m'};
}

# #For DEBUG:
# foreach my $k (keys(%opts)) {
#    print "Clef=$k Valeur=$opts{$k}\n";
# }

# input file
my $file = $opts{'i'};

# Global data
my %int2month =
    (
     '01' => 'jan',
     '02' => 'feb',
     '03' => 'mar',
     '04' => 'apr',
     '05' => 'may',
     '06' => 'jun',
     '07' => 'jul', 
     '08' => 'aug',
     '09' => 'sep',
     '10' => 'oct',
     '11' => 'nov',
     '12' => 'dec'
    );

my @halTypes = 
    (
     'ART',
     'COMM',
     'COUV',
     'THESE',
     'UNDEFINED',
     'OTHER',
     'REPORT',
     'IMG',
     'OUV',
     'MEM',
     'HDR',
     'PATENT',
     'POSTER',
     'LECTURE',
     'PRESCONF',
     'PROCEEDINGS',
    );
# Old HAL types:   
# 'ART_ACL',
# 'ART_SCL',
# 'CONF_INV',
# 'COMM_ACT',
# 'COMM_SACT',
# 'UNDEFINED',
# 'OUVS',
# 'COVS',
# 'DOUV',
# 'PATENT',
# 'OTHER',
# 'REPORT',
# 'THESE',
# 'HDR'

my %hal2aeres = 
    (
     'ART'         => 'ACL',
     'COMM'        => 'ACTI',
     'COUV'        => 'OS',
     'OUV'         => 'DO',
     'THESE'       => 'TH',
     'MEM'         => 'TH',
     'HDR'         => 'TH',
     'UNDEFINED'   => 'AP',
     'OTHER'       => 'AP',
     'REPORT'      => 'AP',
     'IMG'         => 'AP',     
     'PATENT'      => 'AP',
     'POSTER'      => 'AFF',
     'LECTURE'     => 'AP',
     'PRESCONF'    => 'INV',
     'PROCEEDINGS' => 'PROC',
     # Old HAL conversions:
     # 'CONF_INV'  => 'INV',
     # 'COMM_ACT'  => 'ACTI',
     # 'COMM_SACT' => 'COM',
     # 'UNDEFINED' => 'AP',
     # 'OUVS'      => 'OS',
     # 'COVS'      => 'OS',
     # 'DOUV'      => 'DO',
     # 'PATENT'    => 'AP',
     # 'OTHER'     => 'AP',
     # 'REPORT'    => 'AP',
     # 'THESE'     => 'TH',
     # 'HDR'       => 'TH'
    );

my %aeres2code = 
    (
     'ACL'    => '(a)',
     'ACL(N)' => '(b)',
     'ASCL'   => '(c)',
     'ASCL(N)'=> '(d)',
     'DO'     => '(e)',
     'OS'     => '(f)',
     'PROC'   => '(g)',
     'TH'     => '(h)',
     'INV'    => '(i)',
     'ACTI'   => '(j)',
     'ACTN'   => '(k)',
     'AFF'    => '(l)',
     'COM'    => '(m)',
     'AP'     => '(n)'
    );

## NOT AVAILABLE ANY MORE (only keyword-based search)
# my %rel2hal = 
#     (
#      'C'  => 'like',
#      'SB' => 'start_by',
#      'EB' => 'end_by',
#      '='  => 'is_exactly',
#      '>'  => 'gt',
#      '<'  => 'lt',
#      '>=' => 'ge',
#      '<=' => 'le'
#     );

my %op2hal = 
    (
     ' ' => '%20',
     '+' => '+AND+',
     '-' => '+NOT+',
     '|' => '+OR+'
     );

my %kw2hal =
    (
     'author'         => 'authLastName_sci',
     'authorfname'    => 'authFirstName_sci',
     'abstract'       => 'abstract_s',
     'labId'          => 'structId_i',
     'lab'            => 'structAcronym_sci',
     'title'          => 'title_s',
     'year'           => 'producedDateY_i',
     'id'             => 'halId_s'
    );

my @allmetadata = qw(invitedCommunication_s peerReviewing_s audience_s proceedings_s docType_s page_s journalTitle_s bookTitle_s conferenceTitle_s conferenceStartDateY_i conferenceStartDateM_i city_s country_s serie_s volume_s journalPublisher_s publisher_s doiId_s comment_s issue_s producedDate_s authorityInstitution_s description_s); 
# what about file_s ?

my $nbline = 0;

my %outlaws  = (); # for type outlaws
my %doutlaws = (); # for field outlaws (i.e. metadata)

my @alltypes;  # for type restrictions (not any type of article)
my %cou_names; # for translations of country names

# We load the type outlaws
if ($except ne "") {
    open(EXC, "<:utf8",  "$except") or die("Error when opening $except : $!");
    my $e;
    while ($e = <EXC>) {
	chomp($e);
	if ($e ne "") {
	    my ($id,$cat) = split(/;/,$e);
	    $id =~ s/^\s+//;
	    $id =~ s/\s+$//;
	    $cat=~ s/^\s+//;
	    $cat=~ s/\s+$//;
	    #print $id. " " . $cat . "\n";
	    $outlaws{$id} = $cat;
	}
    }
    close(EXC);
}

# We load the field outlaws
if ($dexcept ne "") {
    open(DEXC, "<:utf8",  "$dexcept") or die("Error when opening $dexcept : $!");
    my $e;
    while ($e = <DEXC>) {
	chomp($e);
	if ($e ne "") {
	    my ($id,$field,$newVal) = split(/;/,$e);
	    # newVal being empty means field to be removed
	    $id =~ s/^\s+//;
	    $id =~ s/\s+$//;
	    $field=~ s/^\s+//;
	    $field=~ s/\s+$//;
	    $newVal=~ s/^\s+//;
	    $newVal=~ s/\s+$//;
	    #print $id. ";" . $field . ";" . $newVal . "\n";
	    my $hashref = {};
	    if( exists( $doutlaws{$id} ) ) {
		$hashref = $doutlaws{$id};
	    }
	    else {
		$doutlaws{$id} = $hashref;
	    }
	    #we write the field-value pair (if it already existed, it is replaced)
	    $hashref->{$field} = $newVal;
	}
    }
    close(DEXC);
}

# We load the potential type restriction
if ($types ne "") {
    open(TYP, "<:utf8",  "$types") or die("Error when opening $types : $!");
    my $e;
    while ($e = <TYP>) {
	chomp($e);
	if ($e ne "") {
	    $e =~ s/^\s+//;
	    $e =~ s/\s+$//;
	    push(@alltypes, $e);
	}
    }
    close(TYP);
}

# We load the optional translations of country names
if ($countries ne "") {
    open(COU, "<:utf8",  "$countries") or die("Error when opening $countries : $!");
    my $e;
    while ($e = <COU>) {
	chomp($e);
	if ($e ne "" && $e !~ /^%/) {
	    my ($code,$en) = split(/;/,$e);
	    $code =~ s/^\s+//;
	    $code =~ s/\s+$//;
	    $en =~ s/^\s+//;
	    $en =~ s/\s+$//;
	    #print "DEBUG: $fr -- $en\n";
	    $cou_names{$code} = $en;
	}
    }
    close(COU);
}

# To remember where the php file can be found
my $wdir = File::Spec->rel2abs(dirname($0)); 
chomp($wdir);

##############################################################################
### PHASE 1 : SEARCHING HAL + BUILDING A BIBTEX FILE (UNIQUE PHASE NOW)
##############################################################################

print "SEARCHING HAL\n";

# NB: no more declaration of the webservice nor dynamic creation
# of a php request file (which would use the HAL webservice)
# We now use the SolR Apache search engine (and query language)

# Reading the request from the request file
my $line; 

my @suffixes;
push(@suffixes, '.txt'); 
push(@suffixes, '.tex'); #for outputs 
push(@suffixes, '.req'); #for input requests
push(@suffixes, '.hal');
my ($name,$path) = fileparse($file,@suffixes);

## File::Spec more stable wrt OS than fileparse:
$path = File::Spec->rel2abs(dirname($file));
if ($outrep ne "") {
    $path = $outrep;
}
my $pathname = File::Spec->catfile($path,$name);

my $bibout = $pathname.".bib"; #bibtex output
my $logout = $pathname.".log"; #log file

# Removing existing output files before opening them for writing
if (-e $bibout) {
    (unlink $bibout) or die("Can't remove $bibout : $!");
}

open(REQ, "<:utf8",  "$file") or die("Error when opening $file : $!");
    
if ( $wlog ) {
    if (-e $logout) {
        (unlink $logout) or die("Can't remove $logout : $!");
    }
    open($zelog,">>:utf8","$logout") or die("Error when opening $logout : $!");
    print $zelog "%% Generated using $0 on $time\n\n";
}

open(BIB,">>:utf8","$bibout") or die("Error when opening $bibout : $!");
print BIB "%% Generated using $0 on $time\n\n";
my %memory = (); # to ignore duplicates

# Preparing the parameters

## Fixed parameters (i.e. all kind of publications, ordered by date)
my $typePubli;

if ($types eq "") {
    $typePubli = '('.join("+OR+",@halTypes).')';
}
else {
    $typePubli = '('.join("+OR+",@alltypes).')';
}

if ($maxnum eq "") {
    $maxnum = 200; #default value (having one is enforced by HAL's API)
}

## User-defined parameters 
## NB: One search per line in the request 
##     input file (results are appended)
while( defined( $line = <REQ> ) )
{
    if (($line !~ m/^%/) && ($line ne "\n")) {
        $nbline++;
        
        #print "\nLine being processed : ".$line;
        
        # If the line is neither empty nor commented,
        # we process it to extract the criteria and the 
        # combination operators
        chomp($line); 
	
        my %allCriteria = ();
        
        # We extract the search criteria from the line
        my @textreq = split(/[\+_|]/,$line);
        # We also need the combination operators
        my $a = $line;
        $a =~ s/[^\+_|]//g; # We only keep the join ops
        # print $a."\n"; #DEBUG
        my @combi = split(//,$a);
        # print $#combi."\n"; #DEBUG
        
        for (my $i = 0 ; $i <= $#textreq ; $i++) {
            
            my $criteria = $textreq[$i];
            # The default join operator is '+'
            my $joinop = "+";
            if ($i > 0) {
                $joinop = $combi[$i-1];
                $joinop =~ s/_/-/;
            }
            
            # print "Criteria being processed : ".$criteria." ".$joinop."\n";
            
            # We remove leading and trailing whitespaces
            $criteria =~ s/^\s+//;
            $criteria =~ s/\s+$//;
            
            my ($field,$value) = split(/:/,$criteria);
            $value =~ s/\ /%20/g; # to replace spaces with corresponding HTML code
            #print $field."##".$value."\n"; #DEBUG
            if ( !exists ($allCriteria{$field}) ) {
                $allCriteria{$field} = [];
            } 
            push(@{$allCriteria{$field}}, $joinop.$value);
            
        }
        
        # printing statistics
        print "Request #".$nbline." (".$line."): \n";
        
        # We build the HAL query
        my $allFields = join(',',@allmetadata).",halId_s,title_s,scientificEditor_s,version_i,uri_s,abstract_s"; #not in the metadata because of the entry processing function

        #my $query="https://api.archives-ouvertes.fr/search/hal/?wt=json&q=*:*&fl=".$allFields;
        #my $query="https://api.archives-ouvertes.fr/search/hal/?wt=json&q=*:*&fl=audience_s,title_s,docid,uri_s,docType_s,halId_s"; ## this query causes strange answers
        my $query="https://api.archives-ouvertes.fr/search/hal/?wt=json&q=*:*&fl=*";
        #print $query; ##to print query

        # DEBUG
        my $specialCpt = 0;
        foreach my $keyword (keys(%allCriteria)) {
            # if ($specialCpt == 0) {
            #     print "&q="; 
            # } else {
            #print "&fq="; ##to print query
            $query .= "&fq=";
            # }
            $specialCpt++;
            #print $kw2hal{$keyword}.":("; ##to print query
            $query .= $kw2hal{$keyword}.":(";
            my $nbSpace = 0;
            foreach my $term (@{$allCriteria{$keyword}}) {
                if ($nbSpace > 0) {
                    #here we add $op2hal{$op} where $op is the prefix of $term
                    my @tab = split(//,$term);
                    my $op = shift(@tab);
                    my $zeterm = join('',@tab);
                    #print $op2hal{$op}.""; ##to print query
                    $query .= $op2hal{$op}."";
                    #print $zeterm; ##to print query
                    $query .= $zeterm;
                } else { #the first term is always preceded by the default + operator
                    #print $term; ##to print query
                    $query .= $term;
                }
                $nbSpace++;
            }
            #print ")"; ##to print query
            $query .= ")";
        }
        #print "&fq=docType_s:".$typePubli."&rows=$maxnum\n";  ##to print query
        $query .= "&fq=docType_s:".$typePubli."&rows=$maxnum"; ##$maxnum is an arbitrary number needed to get all the results
        if ($wlog) {
            print $zelog $query."\n";
        }
        ##print $query."\n"; #Debug
        
        ########################################
        ## WE NOW APPLY THE REQUEST IMMEDIATELY
        ########################################
	#my $curl = WWW::Curl::Easy->new;
	#$curl->setopt(CURLOPT_HEADER,0); #Do not include the header in the answer
	#$curl->setopt(CURLOPT_ENCODING, 'gzip');
	#$curl->setopt(CURLOPT_HTTPHEADER, ['Content-Type: text/xml; charset=UTF-8']); #to ensure UTF-8 output
	#$curl->setopt(CURLOPT_URL, $query);
        #my $content="";
	#$curl->setopt(CURLOPT_WRITEDATA,\$content);
	#my $retcode = $curl->perform;

        my $browser = LWP::UserAgent->new;
        my $rep = $browser->get( $query );
        die "Can't get $query -- ", $rep->status_line
            unless $rep->is_success;

        my $retcode = 0;
        my $content = $rep->content;
            
        # Looking at the results...
	if ($retcode == 0) {
            print("Transfer went ok\n");
            #print $content."\n"; ##Debugging

            my $decoded = JSON->new->utf8->decode($content);

	    if ( defined $decoded->{'response'}) {
		my $nbSol = $decoded->{'response'}{'numFound'};
		print "$nbSol document(s) found.\n";
		my @docs = @{ $decoded->{'response'}{'docs'} };
		foreach my $f ( @docs ) {
		    ## For debugging/documentation:
		    # print $f->{'title_s'}[0] . "\n";
		    # print $f->{'docType_s'} . "\n";
		    # if (defined $f->{'audience_s'}) {
		    #     print $f->{'audience_s'} . "\n";
		    # }
		    # if (defined $f->{'label_bibtex'}) {
		    #     print $f->{'label_bibtex'} . "\n";
		    # }

		    ## HAL ENTRY PROCESSING
		    my $bibentry = process_entry(\%memory, $enc, $f, \%outlaws, \%doutlaws, \%cou_names, \%hal2aeres, \%aeres2code, \@allmetadata, $wlog, $zelog, \%int2month);
		    print BIB $bibentry;
		}
            } elsif (defined $decoded->{'error'}) {
		#print Dumper($decoded); ##DEBUG
		print "[ERROR] ".$decoded->{'error'}{'msg'}."\n";
	    }
        } else {
            # Error code, type of error, error message
            print("An error happened\n");
        }
        ###########################
        ### END OF THE PROCESSING
        ###########################
    }
}
close(REQ);
close(BIB);
if ( $wlog ) {
    close($zelog);
}
exit 0;
